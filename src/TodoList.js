import React from 'react';

const listStyle = {
  listStyle: "none",
  border: "1px solid white",
  padding: "10px"
}

export default class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
    this.save = this.save.bind(this);
  }
  handleChange(event) {
    this.setState({value: event.target.value});
  }
  save(event) {
    event.preventDefault()
    this.props.requestCreateTodo(this.state.value)
    this.setState({
      value: '',
    });
  }
  render() {
    return (
      <div className="col-12">
      <div className="container-fluid">
        <div className="row">
        </div>
        <div className="row">
          <form className="col-6 text-center" onSubmit={(event) => this.save(event, this.state.value)}>
            <input name="note" type="text" value={this.state.value} onChange={this.handleChange} />
            <button type="submit">add a note</button>
          </form>
          <div className="col-6">
            <button onClick={this.props.toggleFilter}>{this.props.toggleState} {this.props.name}</button>
          </div>
        </div>
        <div>
          <ul style={listStyle} className="col-12 text-center">
            {
              this.props.notes.map((note, index) => 
              <li key={index}>
                {note.value} 
                 <button onClick={() => this.props.requestDeleteTodo(index)}>✖</button>
              </li>
              )
            }
          </ul>
        </div>
      </div>
      </div>
    );
  }
}