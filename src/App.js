import React from 'react';
import TodoList from './TodoList';
let DBOpenRequest;

// const from = Office.context.mailbox.item._data$p$0._data$p$0.sender.address

class App extends React.Component { 
  constructor(props) {
    super(props); 
    this.state = { 
      email: Office.context.mailbox.item._data$p$0._data$p$0.sender.address,
      name: Office.context.mailbox.item._data$p$0._data$p$0.sender.name,
      notes: [],
      filter: "this conversation with"
     };
      this.createTodo = this.createTodo.bind(this);
      this.deleteTodo = this.deleteTodo.bind(this);
      this.toggleFilter = this.toggleFilter.bind(this);
  }

  // get state from localstorage
  componentDidMount() {
    let persistedItems;
    // load from localstorage
    const key = `mailValet-user-${Office.context.mailbox.item._data$p$0._data$p$0.sender.address}`;
    let persistedUser;
    if(localStorage.getItem(key)) {
      persistedUser = JSON.parse(localStorage.getItem(key))
      this.setState({
        notes: persistedUser.notes,
      })
    } else {
      this.save()
    } 
  }
  createTodo(value) {
    let notes = this.state.notes.slice()
    notes.push({conversationID: Office.context.mailbox.item._data$p$0._data$p$0.conversationId, value})
    this.setState({notes})
    setTimeout(() => this.save())
  }
  deleteTodo(index) {
    let notes = this.state.notes.slice()
    const spliced = notes.splice(index, 1)
    this.setState({notes})
    setTimeout(() => this.save())
  }
  toggleFilter() {
    this.setState({filter: this.state.filter === "all notes with" ? "this conversation with" : "all notes with"})
  }
  save() {
    localStorage.setItem(`mailValet-user-${Office.context.mailbox.item._data$p$0._data$p$0.sender.address}`, 
      JSON.stringify({
        notes: this.state.notes
    }))
  }
  render() {
    return (
      <div className="col-12 ms-bgColor-neutralLighter ms-u-fadeIn500">
        <div className="container-flex">
          <header className="text-center">
            <img src={this.state.avatar} />
            <h3>{this.state.name}</h3>
            <p>{this.state.email}</p>
          </header>
          <div className="row">
            <TodoList 
              name={this.state.name}
              email={this.state.email}
              toggleState={this.state.filter}
              toggleFilter={this.toggleFilter}
              notes={this.state.filter === "all notes with" ? this.state.notes : this.state.notes.filter(el => el.conversationID === Office.context.mailbox.item._data$p$0._data$p$0.conversationId)}
              requestCreateTodo={this.createTodo}
              requestDeleteTodo={this.deleteTodo}
            />
          </div>
        </div>
      </div>
    )
  }
}
export default App;