Here are my notes going along as I developed a plugin for outlook. 

* First obstacle is getting outlook on my Mac for testing - so I downloaded office 360 trial which contains outlook
* Second obstacle was getting an outlook project template into my visual studio editor, which initially is missing 
* Im going to try reinstalling VS to see if I chose not to install the packages - I would rather do this than start by guessing at what the base project looks like
* After the reinstall didn’t add the outlook project template to VS, im moving on to the generic non-VS tutorial
* The yo generator is fine, instantiating a react outlook project. 
* I chose react as your challenge requirements are not extraordinary in any way so I have not many reasons to choose a particular framework beyond generic choices,  and react is a nice way to avoid duplication and spaghetti. ( I hope javascript addins have sufficient API for outlook event listeners though! )
* I instantiate a git project and commit now
* Running the `npm start` command the template seems fine - and the code is in typescript! How microsofty
* so now im gonna pick some mail butler features to make
* Not really sure which features to make yet. 
* Logged into Outlook using a test email address from mailbutler 
* I installed the base react outlook add-in in outlook, it works!
* Googled outlook javascript API - will see what I am capable of doing with the outlook API.
* I finally had my template and everything ready and decided to go with two easy-ish features, adding labels and to-do lists to emails. I will skip calendars, scheduling and so on as that seems to make the scope of the task much bigger. I thought also optionally it would be interested to have some statistics visible such as how many messages have been sent between you and the other primary party, and you and addresses from the same naked domain (eg. Dave@hello.com & hello.com ) 
* So I will need to store data, inject elements in different views, and read through all emails for my desired feature set. I’m now reading https://docs.microsoft.com/en-us/office/dev/add-ins/reference/requirement-sets/outlook-api-requirement-sets to see exactly how things work.
* I set my “API set” to 1.1 and am referencing the 1.6 set documentation, as it’s not too old, and not too new. 
* As my first attempt at seeing / grabbing some data, I’m trying to see Office.context.mailbox.item, but there’s seemingly no console logs or anything, and typescript is making it a pain just to dump the output in the DOM so I can start looking at stuff. Just going to switch to jquery to enable myself to write things to DOM quickly, in the interest of time.
* I like to see things in front of me and to get something directly logging always gives me some confidence, to know I’m in the right place.  “$(‘#app-body').text(`${JSON.stringify(Office.context.mailbox.item)}`)” is pretty dirty but did the trick
* I’d now like to start creating a to-do list so ill add some base react code and will modify webpack to compile JSX using babel
* I have a list loading and im storing it in local storage, works nicely!
* I considered and partially implemented saving avatars into indexeDB but it seems like an unrealistic real-world implementation to spend time on. 
* Sadly I couldn't get the whole user mail list in the Outlook javascript API so im unable to create any stats such as total messages exchanged, which i thought would be interesting to see. 
* I will add a filter to show notes pertaining to threads thread or people
* filter works! pushing to Gitlab



Generally I found this test interesting if not a bit frustrating due to the new environment and lack of clearer examples in the docs. 
I sadly failed to implement the avatar and statistics features that I was interested in, but still made a useful notes application for person and conversation based notes. 
I’d like to experiment with showing statistics between you and the other person, and different client filters like file (attachment) manager views (if that doesn’t already exist) and so on.
If you have any further questions about the job or my code please let me know
